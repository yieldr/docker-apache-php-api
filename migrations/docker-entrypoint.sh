#!/bin/bash
set -e

if [ "$1" = 'apachectl' ]; then

	php /home/code/bin/console migrations:migrate \
		--configuration=/home/code/migrations.yml \
		--no-interaction
fi

exec "$@"